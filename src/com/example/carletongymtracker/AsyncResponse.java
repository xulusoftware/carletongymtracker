package com.example.carletongymtracker;

import java.util.HashMap;

public interface AsyncResponse {
	void processFinish(HashMap<String,String> output);
}
