package com.example.carletongymtracker;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity implements AsyncResponse {

	TextView nv, dv, av, currently, avgDot;
	MenuItem refresh_btn;
	String[] options;
	DrawerLayout drawerLayout;
	ActionBarDrawerToggle mDrawerToggle;
	ListView drawerList;
	int page;
	int currNumOfPeople;
	int averageInt;
	Context mainContext;
	Activity mainActivity;
	private boolean isDrawerOpen = false;
	AsyncResponse a;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		page = 0;
		mainContext = this;
		mainActivity = this;
		Network asyncTask = new Network();
		asyncTask.delegate = this;
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		a = this;
		getActionBar().setTitle("Home");

		// Text Views:
		nv = (TextView) findViewById(R.id.numView);
		dv = (TextView) findViewById(R.id.timeView);
		av = (TextView) findViewById(R.id.averageView);
		avgDot = (TextView) findViewById(R.id.avgDot);
		currently = (TextView) findViewById(R.id.currentLabel);

		if (haveNetworkConnection()) {
			asyncTask.execute(mainContext);
		} else {
			av.setText("An internet connection is required");
		}

		initUI();

	}

	public void initUI() {
		new Thread() {
			@Override
			public void run() {
				// Setting Typefaces
				final Typeface tf = Typeface.createFromAsset(getAssets(),
						"fonts/GS.otf");

				// Drawer:
				ObjectDrawerItem[] drawerItem = new ObjectDrawerItem[2];
				DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(
						mainContext, R.layout.listview_item_row, drawerItem);

				drawerItem[0] = new ObjectDrawerItem(R.drawable.home_32,
						"Home");
				drawerItem[1] = new ObjectDrawerItem(R.drawable.about32,
						"The Developers");
				// drawerItem[2] = new ObjectDrawerItem(R.drawable.ic_launcher,
				// "Fitness Centre News");

				// Eventually want to add Fitness Centre News under options in
				// string.xml
				options = getResources().getStringArray(R.array.options_array);
				drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				drawerList = (ListView) findViewById(R.id.left_drawer);

				// DrawerToggle
				mDrawerToggle = new ActionBarDrawerToggle(mainActivity, /*
																		 * host
																		 * Activity
																		 */
				drawerLayout, /* DrawerLayout object */
				R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
				) {

					public void onDrawerSlide(View drawerView, float slideOffset) {
						if (slideOffset > .55 && !isDrawerOpen) {
							onDrawerOpened(drawerView);
							isDrawerOpen = true;
						} else if (slideOffset < .45 && isDrawerOpen) {
							onDrawerClosed(drawerView);
							isDrawerOpen = false;
						}
					}

					/**
					 * Called when a drawer has settled in a completely closed
					 * state.
					 */
					public void onDrawerClosed(View view) {
						super.onDrawerClosed(view);
						getActionBar().setTitle("Home");
					}

					/**
					 * Called when a drawer has settled in a completely open
					 * state.
					 */
					public void onDrawerOpened(View drawerView) {
						super.onDrawerOpened(drawerView);
						getActionBar().setTitle("CU Gym Traffic");
					}
				};

				// Set the drawer toggle as the DrawerListener
				drawerLayout.setDrawerListener(mDrawerToggle);

				getActionBar().setDisplayHomeAsUpEnabled(true);
				getActionBar().setHomeButtonEnabled(true);

				// new ArrayAdapter<String>(this,R.layout.text_view, options)
				drawerList.setAdapter(adapter);
				drawerList.setItemsCanFocus(false);

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						currently.setTypeface(tf);
						nv.setTypeface(tf);
						av.setTypeface(tf);

						drawerList
								.setOnItemClickListener(new OnItemClickListener() {

									@Override
									public void onItemClick(
											AdapterView<?> parent, View view,
											int pos, long id) {
										if (pos == 0) {

										}
										if (pos == 1) {
											Intent i = new Intent(
													MainActivity.this,
													AboutUs.class);
											startActivity(i);
										}
									}
								});
						mDrawerToggle.syncState();
					}
				});

			}
		}.start();
	}

	public void processFinish(HashMap<String, String> output) {
		nv.setText(output.get("currNum"));
		animateOut();
		dv.setText("At " + output.get("newDate"));
		if(output.get("newDate") == null) {
			dv.setText("");
		}		
		av.setText("Avg Today: " + output.get("averageNum"));
		if(output.get("averageNum") == null) {
			av.setText("No Avg for Today");
		}
		System.out.println("*****DATE: " + output.get("newDate"));
		if(output.get("colour") == null)
		System.out.println("****COLOUR: "+output.get("colour"));
		if(output.get("colour") != null) {
			if (output.get("colour").equalsIgnoreCase("0")) {
				avgDot.setTextColor(Color.BLACK);
			} else if (output.get("colour").equalsIgnoreCase("1")) {
				avgDot.setTextColor(Color.GREEN);
			} else if (output.get("colour").equalsIgnoreCase("2")) {
				avgDot.setTextColor(Color.YELLOW);
			} else if (output.get("colour").equalsIgnoreCase("3")) {
				avgDot.setTextColor(Color.RED);
			}
		}
		setProgressBarIndeterminateVisibility(false);
		refresh_btn.setVisible(true);
		// refresh_btn.setEnabled(true);

	}

	/*
	 * if (averageInt > c) { avgDot.setTextColor(Color.GREEN); } else if
	 * (averageInt == c) { avgDot.setTextColor(Color.YELLOW); } else {
	 * avgDot.setTextColor(Color.RED); }
	 */

	public void animateOut() {
		new Thread() {
			@Override
			public void run() {
				final Animation out = new AlphaAnimation(1.0f, 0.0f);
				out.setDuration(6000);
				out.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationEnd(Animation animation) {
						currently.setAlpha(0.0f);
					}

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation arg0) {
					}
				});
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						currently.startAnimation(out);
					}
				});

			}
		}.start();
	}

	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main_actions, menu);
		refresh_btn = (MenuItem) menu.findItem(R.id.action_refresh);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.action_refresh:
			Network asyncTask = new Network();
			asyncTask.delegate = a;
			if (haveNetworkConnection()) {
				asyncTask.execute(mainContext);
				refresh_btn.setVisible(false);
				setProgressBarIndeterminateVisibility(true);
			} else {
				av.setText("An internet connection is required");
			}
			dv.setAlpha(1.0f);
			currently.setAlpha(1.0f);
			// refresh_btn.setEnabled(false);
			/*
			 * new CountDownTimer(7000, 1000) { public void onTick(long
			 * millisUntilFinished) { }
			 * 
			 * public void onFinish() { } }.start();
			 */
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
