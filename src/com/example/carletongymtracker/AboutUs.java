package com.example.carletongymtracker;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class AboutUs extends Activity {
	
	String[] options;
	DrawerLayout drawerLayout;
	ListView drawerList;
	Activity aboutUsActivity;
	ActionBarDrawerToggle mDrawerToggle;
	private boolean isDrawerOpen = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us);	
		aboutUsActivity = this;
		getActionBar().setTitle("CU Gym Traffic");
		
		// Drawer:
		ObjectDrawerItem[] drawerItem = new ObjectDrawerItem[2];
		DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this,
				R.layout.listview_item_row, drawerItem);

		drawerItem[0] = new ObjectDrawerItem(R.drawable.home_32, "Home");
		drawerItem[1] = new ObjectDrawerItem(R.drawable.about32,
				"The Developers");
		
		options = getResources().getStringArray(R.array.options_array);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = (ListView) findViewById(R.id.left_drawer);
		
		// DrawerToggle
		mDrawerToggle = new ActionBarDrawerToggle(aboutUsActivity, /*
																 * host
																 * Activity
																 */
		drawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		) {

			public void onDrawerSlide(View drawerView, float slideOffset) {
				if (slideOffset > .55 && !isDrawerOpen) {
					onDrawerOpened(drawerView);
					isDrawerOpen = true;
				} else if (slideOffset < .45 && isDrawerOpen) {
					onDrawerClosed(drawerView);
					isDrawerOpen = false;
				}
			}

			/**
			 * Called when a drawer has settled in a completely closed
			 * state.
			 */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				 getActionBar().setTitle("The Developers");
			}

			/**
			 * Called when a drawer has settled in a completely open
			 * state.
			 */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActionBar().setTitle("CU Gym Traffic");
			}
		};

		// Set the drawer toggle as the DrawerListener
		drawerLayout.setDrawerListener(mDrawerToggle);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// new ArrayAdapter<String>(this,R.layout.text_view, options)
		drawerList.setAdapter(adapter);
		drawerList.setItemsCanFocus(false);

				drawerList.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(
									AdapterView<?> parent, View view,
									int pos, long id) {
								if (pos == 0) {
									Intent i = new Intent(
											AboutUs.this,
											MainActivity.class);
									startActivity(i);
								}
								if (pos == 1) {
									
								}
							}
						});
				mDrawerToggle.syncState();
	}
}
