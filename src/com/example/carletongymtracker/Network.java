package com.example.carletongymtracker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

@SuppressLint("SimpleDateFormat")
public class Network extends AsyncTask<Context, Void, HashMap<String, String>> {
	
	public AsyncResponse delegate = null;

    @Override
    protected void onPreExecute() {
        //dialog.show();
    }

	@Override
	protected HashMap<String, String> doInBackground(Context... arg0) {
		// TODO Auto-generated method stub
		Parse.initialize(arg0[0], "1x7TBWCgmegporCPvvPo5kEgc6cbED7QGgFc7wM8",
				"9RE9pDzYQ4d98KOW3akeblS0aFIZUz7ZmrGWClgq");
		HashMap<String, String> information = retrieve();
		return information;
	}

	@Override
	protected void onPostExecute(HashMap<String, String> result) {
		//dialog.dismiss();
		delegate.processFinish(result);
	}

	@SuppressWarnings("deprecation")
	public HashMap<String, String> retrieve() {
		final HashMap<String, String> hashMap = new HashMap<String, String>();

		// Setting up the Parse Query
		final ParseQuery<ParseObject> peopleQuery = ParseQuery
				.getQuery("GymCount");
		peopleQuery.orderByDescending("updatedAt");

		// Creating Date variables to define a 24 hour period
		final Date midnight = new Date();
		midnight.setHours(0);
		midnight.setMinutes(0);
		midnight.setSeconds(0);
		final Date elevenfiftynine = new Date();
		elevenfiftynine.setHours(23);
		elevenfiftynine.setMinutes(59);
		elevenfiftynine.setSeconds(59);

		// Sorting the Query to retrieve only logs for TODAY
		peopleQuery.whereGreaterThan("createdAt", midnight);
		peopleQuery.whereLessThan("createdAt", elevenfiftynine);

		// Retrieving latest number and date
		try {
			// Put the current num of people in the hashMap
			int currInt = (int) peopleQuery.getFirst().getDouble("NumOfPeople");
			String currNum = currInt + "";
			hashMap.put("currNum", currNum);

			// Getting the time updated for the current num of people
			Date date = peopleQuery.getFirst().getUpdatedAt();
			String newDate = new SimpleDateFormat("hh:mm aa").format(date);
			hashMap.put("newDate", newDate);

			// Retrieving the average number of people
			List<ParseObject> objects = peopleQuery.find();
			int averageInt = 0;
			String averageNum;

			for (int i = 0; i < objects.size(); i++) {
				averageInt += objects.get(i).getInt("NumOfPeople");
			}
			if (objects.size() != 0) {
				averageInt = averageInt / objects.size();
				averageNum = averageInt + "";
			} else {
				averageNum = "0";
			}
			hashMap.put("averageNum", averageNum);

			// Retrieving colour value for the average dot
			hashMap.put("colour", compare(currInt, averageInt));

		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return hashMap;
	}

	public String compare(int currInt, int averageInt) {
		int colour = 0;
		if (currInt > 0 && averageInt > 0) {
			if (averageInt > currInt) {
				colour = 1;
			} else if (averageInt == currInt) {
				colour = 2;
			} else if (averageInt < currInt){
				colour = 3;
			}
		}
		return colour + "";
	}
}
