import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class Graphic
{
	GraphicsEnvironment ge;
	Font modern;
	Color grey, red, redClicked;
	JButton save, clear;

	public void displayWindow()
	{
		try
		{
			modern = Font.createFont(Font.TRUETYPE_FONT, new File("assets/fonts/fnt.ttf"))
					.deriveFont(12f);
			ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(modern);
		}
		catch (IOException | FontFormatException e)
		{
			// Handle exception
		}

		JFrame frame = new JFrame("Carleton Gym App");
		save = new JButton("Save");
		clear = new JButton("Clear");
		JLabel lastUpdate = new JLabel("Updated");
		IntegerTextField numberOfPeople = new IntegerTextField();

		grey = new Color(0x3B3738);
		red = new Color(0xC63D0F);
		redClicked = new Color(0xDD8B6F);

		// Frame specific details
		frame.setLayout(null);
		frame.setSize(220, 150);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setBackground(grey);
		frame.getContentPane().setBackground(grey);

		// Field to enter number of people
		numberOfPeople.setLocation(35, 50);
		numberOfPeople.setHorizontalAlignment(JTextField.CENTER);
		numberOfPeople.setSize(150, 23);
		frame.add(numberOfPeople);

		// Save button to save people
		save.setText("Save");
		save.setFont(modern);
		save.setLocation(35, 80);
		save.setSize(70, 23);
		save.setBorder(new LineBorder(Color.BLACK));
		save.setBackground(red);
		save.setOpaque(true);
		save.addMouseListener(new MouseAdapter()
		{
			private boolean mouseDown = false;

			public void mousePressed(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1)
				{
					mouseDown = true;
					initThread();
					Data sendData = new Data();
					sendData.newObject();
					// Create an instance of SimpleDateFormat used for
					// formatting
					// the string representation of date (month/day/year)
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

					// Get the date today using Calendar object.
					Date today = Calendar.getInstance().getTime();
					// Using DateFormat format method we can create a string
					// representation of a date with the defined format.
					String reportDate = df.format(today);

					// Print what date is today!
					System.out.println("Report Date: " + reportDate);
					
					sendData.newData(reportDate, number);
				}
			}

			public void mouseReleased(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1)
				{
					mouseDown = false;
				}
			}

			private boolean isRunning = false;

			private synchronized boolean checkAndMark()
			{
				if (isRunning)
					return false;
				isRunning = true;
				return true;
			}

			private void initThread()
			{
				if (checkAndMark())
				{
					new Thread()
					{
						public void run()
						{
							do
							{
								save.setBackground(redClicked);
							}
							while (mouseDown);
							isRunning = false;
							save.setBackground(red);
						}
					}.start();
				}
			}
		});
		frame.add(save);

		// Clear button to clear number people
		clear.setText("Clear");
		clear.setFont(modern);
		clear.setLocation(116, 80);
		clear.setSize(70, 23);
		clear.setBorder(new LineBorder(Color.BLACK));
		clear.setBackground(red);
		clear.setOpaque(true);
		clear.addMouseListener(new MouseAdapter()
		{
			private boolean mouseDown = false;

			public void mousePressed(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1)
				{
					mouseDown = true;
					initThread();
				}
			}

			public void mouseReleased(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1)
				{
					mouseDown = false;
				}
			}

			private boolean isRunning = false;

			private synchronized boolean checkAndMark()
			{
				if (isRunning)
					return false;
				isRunning = true;
				return true;
			}

			private void initThread()
			{
				if (checkAndMark())
				{
					new Thread()
					{
						public void run()
						{
							do
							{
								clear.setBackground(redClicked);
							}
							while (mouseDown);
							isRunning = false;
							clear.setBackground(red);
						}
					}.start();
				}
			}
		});
		frame.add(clear);

		// Label to display
		lastUpdate.setText("Last Update: 130 at 1:30pm");
		lastUpdate.setFont(modern);
		lastUpdate.setFont(lastUpdate.getFont().deriveFont(10f));
		lastUpdate.setForeground(Color.white);
		lastUpdate.setLocation(41, 28);
		lastUpdate.setSize(150, 23);
		frame.add(lastUpdate);

		frame.setVisible(true);
	}

	// This a custom JTextField to only allow numbers
	public class IntegerTextField extends JTextField
	{

		private static final long serialVersionUID = 2817574497079218001L;
		final static String badchars = "`~!@#$%^&*()_+=\\|\"':;?/>.<, ";

		public void processKeyEvent(KeyEvent ev)
		{
			char c = ev.getKeyChar();
			if ((Character.isLetter(c) && !ev.isAltDown()) || badchars.indexOf(c) > -1)
			{
				ev.consume();
				return;
			}
			if (getDocument().getLength() > 2)
				ev.consume();
			if (c == '-' && getDocument().getLength() > 0)
				ev.consume();
			else
				super.processKeyEvent(ev);
		}
	}
}
